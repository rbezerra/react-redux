export const deletePost = (id) => {
    return {
        type: 'DELETE_POST',
        id
    }
}

export const addPost = (newPost) => {
    return {
        type: 'ADD_POST',
        newPost
    }
}