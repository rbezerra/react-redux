import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addPost} from './../actions/postActions';

class AddPost extends Component {

    state = {
        title: null,
        body: null
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const newPost = {
            title: this.state.title,
            body: this.state.body
        };
        this.props.addPost(newPost);
        this.props.history.push('/');
    }

    handleChange = (e) => {
        this.setState({[e.target.id] : e.target.value});
    }

    render() {
        return (
            <div className="container">
                <h4 className="center">Add Post</h4>
                <form className="col s12" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="input-field col s12">
                            <label htmlFor="tile">Title</label>
                            <input type="text" id="title" name="title" className="validate" onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <textarea id="body" name="body" cols="30" rows="10" className="materialize-textarea" onChange={this.handleChange}></textarea>
                            <label htmlFor="body">Body</label>
                        </div>
                    </div>
                    <div className="row center">
                        <button className="btn green">SAVE</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        props: state.props 
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
         addPost: (newPost) => {
            dispatch(addPost(newPost))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);