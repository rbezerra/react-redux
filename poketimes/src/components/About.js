import React from 'react';
import Rainbow from '../components/hoc/Rainbow';

const About = () => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sunt explicabo in ullam eum quia rerum rem ipsam consequuntur aspernatur, quisquam beatae ducimus quibusdam harum quas amet, mollitia deleniti nulla esse.</p>
        </div>
    )
}

export default Rainbow(About);